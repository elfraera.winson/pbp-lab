from django.contrib import admin

# Registering Friend model
from .models import Friend
admin.site.register(Friend)
