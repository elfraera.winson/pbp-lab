from django.db import models

# Creating Friend model that contains name, npm, and DOB (date of birth)

class Friend(models.Model):
    name = models.CharField(max_length=30)
    # Implementing missing attributes in Friend model
    npm = models.CharField(max_length=30)
    date_of_birth = models.DateField()

    # Additional
    def __str__(self):
        return "{}".format(self.name)
