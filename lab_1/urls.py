from lab_1.models import Friend
from django.urls import path
from .views import friend_list, index

urlpatterns = [
    path('', index, name='index'),
    # Adding friends path using friend_list Views
    path('friends', friend_list, name='friend_list')
    
]
