from django.db import models

# Create your models here.
class Note(models.Model):
    kepada = models.CharField(max_length=20)
    dari = models.CharField(max_length=20)
    title = models.CharField(max_length=30)
    message = models.TextField()

    def __str__(self):
        return "{}".format(self.title)
