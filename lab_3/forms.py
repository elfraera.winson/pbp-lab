from django import forms
from lab_1.models import Friend

# Create your models here.
class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'date_of_birth']
        
        pesan_error = {'required':'Please Fill'}
        masukan_pertama = {'type':'text', 'placeholder':'Input Your Name Here'}
        masukan_kedua = {'type':'number', 'placeholder':'Input Your NPM Here'}
        masukan_ketiga = {'type':'date'}
               
        name = forms.CharField(label = '', required = True, max_length = 100, widget = forms.TextInput(attrs = masukan_pertama))
        npm = forms.CharField(label = '', required = True, max_length = 100, widget = forms.NumberInput(attrs = masukan_kedua))
        date_of_birth = forms.CharField(label = '', required = True, widget = forms.DateInput(attrs = masukan_ketiga))
