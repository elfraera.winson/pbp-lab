from lab_1.views import calculate_age
from django.shortcuts import render, HttpResponseRedirect
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login')
def index(request):
    friends = Friend.objects.all()
    response = {'friends':friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login')
def add_friend(request):
    context = {}
    form = FriendForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return HttpResponseRedirect('/lab-3')
    else:
        context['form'] = form
        return render(request, 'lab3_form.html', context)

