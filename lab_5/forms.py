from django import forms
from lab_2.models import Note

# Create your models here.
class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['kepada', 'dari', 'title', 'message']
        
        pesan_error = {'required':'Please Fill'}
        masukan_pertama = {'type':'text', 'placeholder':'Input Your "From" Here'}
        masukan_kedua = {'type':'text', 'placeholder':'Input Your "To" Here'}
        masukan_ketiga = {'type':'text', 'placeholder':'Input Your "Title" Here'}
        masukan_keempat = {'type':'text', 'placeholder':'Input Your "Message" Here'}
               
        kepada = forms.CharField(label = '', required = True, max_length = 75, widget = forms.TextInput(attrs = masukan_pertama))
        dari = forms.CharField(label = '', required = True, max_length = 75, widget = forms.NumberInput(attrs = masukan_kedua))
        title = forms.CharField(label = '', required = True, max_length = 150, widget = forms.TextInput(attrs = masukan_ketiga))
        message = forms.CharField(label = '', required = True, max_length = 1000, widget = forms.TextInput(attrs = masukan_keempat))
    