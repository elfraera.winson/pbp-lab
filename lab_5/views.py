from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponse
from django.core import serializers
from .forms import NoteForm

def index(request):
    notes = Note.objects.all()  
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def get_note(request):
    data = serializers.serialize('json', Note.objects)
    return HttpResponse(data, content_type="application/json")

def update_note(request):
    context = {}
    form = NoteForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return HttpResponse(status = 200)
    else:
        context['form'] = form
        return render(request, 'lab5_index.html', context)

def delete_note(request):

