import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';

void main() => runApp(const ReplyForum());

/// This is the main application widget.
class ReplyForum extends StatelessWidget {
  const ReplyForum({Key? key}) : super(key: key);

  static const String _title = 'D06 Flutter Project';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      home: BottomNavBar(),
    );
  }
}

// class Forum extends StatefulWidget {
//   const Forum({Key? key}) : super(key: key);
//
//   @override
//   _ForumState createState() => _ForumState();
// }

class Forum extends StatefulWidget {
  const Forum({Key? key}) : super(key: key);

  @override
  ForumState createState() {
    return ForumState();
  }
}

class ForumState extends State<Forum> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: const Color(0xFFeae4d1),
      title: 'D06 Flutter Project',
      home: Scaffold(
        backgroundColor: const Color(0xFFeae4d1),
        appBar: AppBar(
          title: const Text('Your New Post'),
          backgroundColor: const Color(0xFF6d1404),
        ),
        body: const MyCustomForm(),
      ),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            decoration: new InputDecoration(
              labelText: 'Enter title',
              fillColor: const Color(0xFFeae4d1),
              border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(30.0),
                borderSide: new BorderSide(),
              ),
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter the title';
              }
              return null;
            },
          ),
          TextFormField(
            decoration: new InputDecoration(
              labelText: 'Enter content',
              fillColor: const Color(0xFFeae4d1),
              border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(30.0),
                borderSide: new BorderSide(),
              ),
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter the content';
              }
              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: const Color(0xFF6d1404),
                onPrimary: Colors.white,
              ),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Your post has been saved')),
                  );
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const BottomNavBar()),
                  );
                }
              },
              child: const Text('Submit'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: const Color(0xFF6d1404),
                onPrimary: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const BottomNavBar()),
                );
              },
              child: const Text('Cancel'),
            ),
          ),
        ],
      ),
    );
  }
}

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({Key? key}) : super(key: key);

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int currentPage = 0;
  static const TextStyle optionStyle = TextStyle(fontSize: 50, fontWeight: FontWeight.bold);
  static TextStyle optionStyle2 = TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.grey.shade900);
  static const List<Widget> tabs = [
    Center(child: Text("Home", style: optionStyle)),
    Center(child: Text("Login Page", style: optionStyle)),
    Center(child: Text("Workout", style: optionStyle)),
    Center(child: Text("Recipe", style: optionStyle)),
    Center(child: Text("Covid-19", style: optionStyle)),
    Center(child: Text("Forum", style: optionStyle)),
  ];

  setPage(index) {
    setState(() {
      currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFeae4d1),
      body: Container(
        padding: EdgeInsets.all(1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20,),
            FadeInDown(
              from: 200,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: () => setPage(0),
                    icon: Icon(Icons.home, size: 25, color: currentPage == 0 ? Colors.black : Colors.grey.shade500,),
                  ),
                  Text("Mobile App Project", style: optionStyle2,),
                  IconButton(
                    onPressed: () => setPage(1),
                    icon: Icon(Icons.logout, size: 25, color: currentPage == 1 ? Colors.black : Colors.grey.shade500,),
                  ),
                ],
              ),
            ),
            SizedBox(height: 300,),
            FadeInUp(
              from: 200,
              child: tabs[currentPage],
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: const Color(0xFF6d1404),
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 50,
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                onPressed: () => setPage(2),
                icon: Icon(Icons.sports_tennis, size: 25, color: currentPage == 2 ? Colors.white : Colors.grey.shade500,),
              ),
              IconButton(
                onPressed: () => setPage(3),
                icon: Icon(Icons.favorite_border, size: 25, color: currentPage == 3 ? Colors.white : Colors.grey.shade500,),
              ),
              IconButton(
                onPressed: () => setPage(4),
                icon: Icon(Icons.notifications_outlined, size: 25, color: currentPage == 4 ? Colors.white : Colors.grey.shade500,),
              ),
              IconButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const Forum()),
                  );
                },
                icon: Icon(Icons.people_outlined, size: 25, color: currentPage == 5 ? Colors.white : Colors.grey.shade500,),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
